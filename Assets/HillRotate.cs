﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HillRotate : MonoBehaviour {

    public float rotationValue;
    public float maxAngle;
    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        float xAng = limitAngles(gameObject.transform.localEulerAngles);
        float xRot = 0;
        if (Input.GetKey(KeyCode.LeftArrow))
            xRot -= rotationValue;
        else if (Input.GetKey(KeyCode.RightArrow))
            xRot += rotationValue;
        if ((xAng > maxAngle && xRot > 0) || (xAng < -maxAngle && xRot < 0))
            xRot = 0;
        gameObject.transform.Rotate(new Vector3((xRot * 60 * Time.deltaTime), 0 , 0));
    }

    float limitAngles(Vector3 angles) {
        if (angles.x > 180)
            angles.x -= 360;
        else if (angles.x < -180)
            angles.x += 360;
        return angles.x;
    }
}

