﻿using UnityEngine;
using System.Collections;

public class PlaneRotate : MonoBehaviour {
    public float rotationAmount;
    public GameObject ball;
    const int rightAngle = 90;
    const int behindAngle = 180;

    // Use this for initialization
    void Start() {
        
    }

    Vector3 limitAngles(Vector3 angles) {
        if (angles.x > 180)
            angles.x -= 360;
        else if (angles.x < -180)
            angles.x += 360;
        if (angles.z > 180)
            angles.z -= 360;
        else if (angles.z < -180)
            angles.z += 360;
        return angles;
    }

    Vector3 keyVector(Vector3 camFacing) {
        Vector3 rotVec = new Vector3(0, 0, 0);
        if (Input.GetKey(KeyCode.UpArrow))
            rotVec += camFacing;
        if (Input.GetKey(KeyCode.DownArrow))
            rotVec += (Quaternion.Euler(0, behindAngle, 0) * camFacing);
        if (Input.GetKey(KeyCode.LeftArrow))
            rotVec += (Quaternion.Euler(0, -rightAngle, 0) * camFacing);
        if (Input.GetKey(KeyCode.RightArrow))
            rotVec += (Quaternion.Euler(0, rightAngle, 0) * camFacing);
        return rotVec.normalized;
    }

    Vector3 fixYRot(Vector3 planeAngles, Vector3 rotVec) {
        if (Mathf.Abs(planeAngles.y) > 0)
            rotVec.y = -planeAngles.y;
        return rotVec;
    }

    // Update is called once per frame
    void Update() {
        Vector3 Angles = limitAngles(gameObject.transform.eulerAngles);
        Vector3 camFacing = ball.GetComponent<CameraScript>().distanceVec();
        camFacing.y = 0;
        camFacing.Normalize();
        Vector3 rotVec = keyVector(camFacing) * (60 * rotationAmount * Time.deltaTime);
        Vector3 test = Quaternion.Euler(0, Vector3.SignedAngle(rotVec, gameObject.transform.position, Vector3.up), 0) * rotVec;
        if ((Angles.x > 5 && test.x > 0) || (Angles.x < -5 && test.x < 0))
            test.x = 0;
        if ((Angles.z > 5 && test.z > 0) || (Angles.z < -5 && test.z < 0))
            test.z = 0;
        test.y = -gameObject.transform.eulerAngles.y;
        gameObject.transform.Rotate(test);
    }
}
