﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;

public class ScoreGoal : MonoBehaviour {

    public HillCamera pcs;
    public TextMeshProUGUI tmp;
    private bool triggered = false;
    private float time = 0;

    private void Update() {
        if (triggered) {
            time += Time.deltaTime;
            if (time > 3) {
                returnToMain();
            }
        }
    }

    //When the player reaches the goal run, comapre time and exit
    void OnTriggerEnter(Collider other) {
        int tries = pcs.getScore();
        tmp.SetText("Congratulations you reached the Goal in " + tries + " tr" + (tries == 1 ? "y" : "ies"));
        compareTries(tries);
        triggered = true;
    }

    //Compares the newTime against the current best, and if the new time is better stores it
    private void compareTries(int tries) {
        int bestScore = PlayerPrefs.GetInt("Tries");
        if (tries > bestScore && bestScore > 0) return;
        else
            PlayerPrefs.SetInt("Tries", tries);

    }


    //Seperate so the pause screen can access the function without a seperate object
    public void returnToMain() {
        SceneManager.LoadScene(0);
    }
}

