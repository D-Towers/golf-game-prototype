﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class sceneSwap : MonoBehaviour {
    // Update is called once per frame
    public void SceneLoader(int index) {
        SceneManager.LoadScene(index);
        Time.timeScale = 1.0f;
    }

    public void AppQuit() {
        Application.Quit();
    }
}
