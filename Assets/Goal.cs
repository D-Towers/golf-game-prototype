﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;

public class Goal : MonoBehaviour {

    public CameraScript pcs;
    public TextMeshProUGUI tmp;
    private bool triggered = false;
    private float time = 0;

    private void Update() {
        if (triggered) {
            time += Time.deltaTime;
            if (time > 3) {
                returnToMain();
            }
        }
    }

    //When the player reaches the goal run, comapre time and exit
    void OnTriggerEnter(Collider other) {
        string time = pcs.getTimeString();
        tmp.SetText("Congratulations you reached the Goal in " + time);
        compareTimes(time);
        triggered = true;
    }

    //Compares the newTime against the current best, and if the new time is better stores it
    private void compareTimes(string newTime) {
        string bestTime = PlayerPrefs.GetString("Time");
        Vector2Int nt = retrieveTime(newTime);
        Vector2Int bt = retrieveTime(bestTime);
        if (nt.x > bt.x) return;
        else {
            if (nt.y >= bt.y) return;
            PlayerPrefs.SetString("Time", newTime);
        }
    }

    //For a given string in the format /(\d+:)?\d{2}/ split it into a Vector2Int holding minutesin x and seconds in y
    Vector2Int retrieveTime(string time) {
        Vector2Int timeVec = new Vector2Int();
        if (time.Length == 2) {
            timeVec.x = 0;
            timeVec.y = int.Parse(time);
        }
        else {
            string[] strings = time.Split(':');
            timeVec.x = int.Parse(strings[0]);
            timeVec.y = int.Parse(strings[1]);
        }
        return timeVec;
    }


    //Seperate so the pause screen can access the function without a seperate object
    public void returnToMain() {
        SceneManager.LoadScene(0);
    }
}

