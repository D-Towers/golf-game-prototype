﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class CameraScript : MonoBehaviour {
    // Start is called before the first frame update
    private Camera cam;
    public TextMeshProUGUI text;
    public GameObject panel;
    private float time = 0;
    private int minutes = 0;
    private int seconds = 0;

    void Start() {
        cam = Camera.main;
    }
    
    //Returns a Vector2Int containing minutes in the x component, and seconds in the y component
    private void updateTime() {
        time += Time.deltaTime;
        minutes = Mathf.FloorToInt(time / 60);
        seconds = Mathf.FloorToInt(time % 60);
    }

    public Vector2Int getTime() {
        return new Vector2Int(minutes, seconds);
    }
    public string getTimeString() {
        return (minutes > 0 ? minutes + ":" : "") + (seconds < 10 ? "0" + seconds : seconds.ToString());
    }

    // Update is called once per frame
    void Update() {
        if (Time.timeScale != 0) {
            updateTime();
            text.SetText("Time Taken: " + getTimeString());
            cam.transform.LookAt(gameObject.transform);
            float distance = Vector3.Distance(cam.transform.position, gameObject.transform.position);
            cam.GetComponent<Rigidbody>().velocity = Vector3.zero;
            if (distance > 5) {
                cam.GetComponent<Rigidbody>().AddForce(normDistanceVec() * (-2 * (distance - 5)) / Time.deltaTime);
            }

            RaycastHit plane;
            if (Physics.Raycast(cam.transform.position, Vector3.down, out plane, (1 << 8))) {
                Debug.DrawRay(cam.transform.position, Vector3.down * plane.distance, Color.yellow, 0.5f);
                if (plane.distance < 2) {
                    cam.GetComponent<Rigidbody>().AddForce(Vector3.up * (-2 * (plane.distance - 2)) / Time.deltaTime);
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            TogglePauseScreen();
        }
    }

    public void TogglePauseScreen() {
        if (panel.activeSelf)
            Time.timeScale = 1.0f;
        else 
            Time.timeScale = 0;
        panel.SetActive(!panel.activeSelf);
    }

    public Vector3 distanceVec() {
        return  gameObject.transform.position - cam.transform.position;
    }

    public Vector3 distanceVecFromBall() {
        return cam.transform.position - gameObject.transform.position ;
    }

    Vector3 normDistanceVec() {
        return distanceVecFromBall().normalized;
    }
}