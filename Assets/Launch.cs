﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Launch : MonoBehaviour {
    public int x;
    public int y;
    public float startX;
    public TextMeshProUGUI tmp;

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        if (gameObject.transform.position.x == startX && Input.GetKeyDown(KeyCode.Space)) {
            gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(-x, y, 0));
            gameObject.GetComponent<HillCamera>().incScore();
            tmp.SetText("");
        }
    }
}
