﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class HillCamera : MonoBehaviour {
    // Start is called before the first frame update
    private Camera cam;
    public TextMeshProUGUI text;
    public GameObject panel;
    private int score = 0;

    public int getScore() {
        return score;
    }
    public void incScore() {
        score++;
    }

    private void Start() {
        cam = Camera.main;
    }
    // Update is called once per frame
    void Update() {
        if (Time.timeScale != 0) {
            text.SetText("Score: " + getScore());
            Vector3 noZ = gameObject.transform.position;
            noZ.z = 0;
            cam.transform.LookAt(noZ);
            Vector3 camNoZ = cam.transform.position;
            camNoZ.z = 0;
            float distance = Vector3.Distance(camNoZ, noZ);
            cam.GetComponent<Rigidbody>().velocity = Vector3.zero;
            if (distance > 5) {
                cam.GetComponent<Rigidbody>().AddForce(normDistanceVecNoZ() * (-2 * (distance - 5)) / Time.deltaTime);
            }

            RaycastHit plane;
            if (Physics.Raycast(cam.transform.position, Vector3.down, out plane, (1 << 8))) {
                Debug.DrawRay(cam.transform.position, Vector3.down * plane.distance, Color.yellow, 0.5f);
                if (plane.distance < 2) {
                    cam.GetComponent<Rigidbody>().AddForce(Vector3.up * (-2 * (plane.distance - 2)) / Time.deltaTime);
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            TogglePauseScreen();
        }
    }

    public void TogglePauseScreen() {
        if (panel.activeSelf)
            Time.timeScale = 1.0f;
        else
            Time.timeScale = 0;
        panel.SetActive(!panel.activeSelf);
    }

    public Vector3 distanceVec() {
        return gameObject.transform.position - cam.transform.position;
    }

    public Vector3 distanceVecFromBallNoZ() {
        Vector3 disVec = cam.transform.position - gameObject.transform.position;
        disVec.z = 0;
        return disVec;
    }

    Vector3 normDistanceVecNoZ() {
        return distanceVecFromBallNoZ().normalized;
    }
}