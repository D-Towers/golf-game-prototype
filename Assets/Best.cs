﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Best : MonoBehaviour {
    public string measurement;
    // Start is called before the first frame update
    void Start() {
        if (measurement == "Time") {
            string val = PlayerPrefs.GetString(measurement);
            if (val != "")
                gameObject.GetComponent<TextMeshProUGUI>().SetText("Best Time: " + val);
            else
                gameObject.SetActive(false);
        }
        else if (measurement == "Tries") {
            int val = PlayerPrefs.GetInt(measurement);
            if (val > 0)
                gameObject.GetComponent<TextMeshProUGUI>().SetText("Fewest Tries: " + PlayerPrefs.GetInt(measurement));
            else
                gameObject.SetActive(false);
        }
    }
}
